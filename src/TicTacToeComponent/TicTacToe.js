import React, { useEffect } from 'react';
import './TicTacToe.css';
import { useSelector, useDispatch } from 'react-redux';
import { moveIncrement, gameCountIncrement, pushToMatchHistory, fill00, fill01, fill02, fill10, fill11, fill12, fill20, fill21, fill22, setFirstMove, displayWin, trackCurrentPlayer, reset, changePlayerName} from '../actions/index';
import MatchHistory from '../MatchHistoryComponent/MatchHistory'




function TicTacToe() {
    const tableArray = useSelector(state => state.tableArray);
    const playerNames = useSelector(state => state.playerNames);
    const firstMove = useSelector(state => state.firstMove);
    const isWin = useSelector(state => state.displayWin);
    const currentPlayer = useSelector(state => state.currentPlayer)
    const gameCount = useSelector(state => state.gameCount)
    const move = useSelector(state => state.firstMove);
    const moveCount = useSelector(state => state.moveCount)
    const dispatch = useDispatch();
    var isDraw = false;

    function checkWin(a, b, c){
        if(a !== "" && b !== "" && c !== "" ){
            if (a === b && b === c && a === c){
                return true;
            }
        }
        return false;
    }

    function handleChangePlayer1(event){
        dispatch(changePlayerName('Player 1', event.target.value));
    }
    function handleChangePlayer2(event){
        dispatch(changePlayerName('Player 2', event.target.value));
    }


    if (tableArray.map(e => e.join("")).join("") === "") {
        dispatch(displayWin(false));
    }
    else{
        if(checkWin(tableArray[0][0], tableArray[0][1], tableArray[0][2])
        || checkWin(tableArray[1][0], tableArray[1][1], tableArray[1][2])
        || checkWin(tableArray[2][0], tableArray[2][1], tableArray[2][2])
        || checkWin(tableArray[0][0], tableArray[1][0], tableArray[2][0])
        || checkWin(tableArray[0][1], tableArray[1][1], tableArray[2][1])
        || checkWin(tableArray[0][2], tableArray[1][2], tableArray[2][2])
        || checkWin(tableArray[0][0], tableArray[1][1], tableArray[2][2])
        || checkWin(tableArray[0][2], tableArray[1][1], tableArray[2][0])){
            dispatch(displayWin(true));
        }
    }

    isDraw = !isWin && (tableArray.map(e => e.join("")).join("").length === 9)

    function pushifWinOrDraw(){
        if (isDraw){
            let payload = {
                gameCount: gameCount,
                winner: "Draw",
                movesTaken: "N/A",
                move: null
            }
            dispatch(pushToMatchHistory(payload));
            dispatch(gameCountIncrement());
        }
        else if(isWin){
            let payload = {
                gameCount: gameCount,
                winner: currentPlayer === playerNames.player1Name ? playerNames.player2Name : playerNames.player1Name,
                movesTaken: Math.round(moveCount/2),
                move: move === "O" ? "X" : "O"
            }
            dispatch(pushToMatchHistory(payload))
            dispatch(gameCountIncrement());
        }
    }

    useEffect(() => {
        pushifWinOrDraw();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isWin, isDraw]);

    return (
        <div>
            {firstMove.length !== 0 ? 
            <div>
                <table>
                <tr>
                    <td 
                    onClick={() => 
                    {dispatch(fill00(firstMove, isWin)); 
                    dispatch(setFirstMove(firstMove, tableArray[0][0]));
                    dispatch(trackCurrentPlayer(isWin, tableArray[0][0], playerNames));
                    dispatch(moveIncrement());
                    }}
                    >
                        {tableArray[0][0]}
                    </td>
                    <td 
                    className="vert" 
                    onClick={() => 
                    {dispatch(fill01(firstMove, isWin)); 
                    dispatch(setFirstMove(firstMove, tableArray[0][1]));
                    dispatch(trackCurrentPlayer(isWin, tableArray[0][1], playerNames));
                    dispatch(moveIncrement())
                    }}>
                        {tableArray[0][1]}
                    </td>
                    <td 
                    onClick={() => 
                    {dispatch(fill02(firstMove, isWin)); 
                    dispatch(setFirstMove(firstMove, tableArray[0][2]));
                    dispatch(trackCurrentPlayer(isWin, tableArray[0][2], playerNames));
                    dispatch(moveIncrement())
                    }}>
                        {tableArray[0][2]}
                    </td>
                </tr>
                <tr>
                    <td 
                    className="hori" 
                    onClick={() => 
                    {dispatch(fill10(firstMove, isWin)); 
                    dispatch(setFirstMove(firstMove, tableArray[1][0]));
                    dispatch(trackCurrentPlayer(isWin, tableArray[1][0], playerNames));
                    dispatch(moveIncrement())
                    }}>
                        {tableArray[1][0]}
                    </td>
                    <td 
                    className="vert hori" 
                    onClick={() => 
                    {dispatch(fill11(firstMove, isWin)); 
                    dispatch(setFirstMove(firstMove, tableArray[1][1]));
                    dispatch(trackCurrentPlayer(isWin, tableArray[1][1], playerNames));
                    dispatch(moveIncrement())
                    }}>
                        {tableArray[1][1]}
                    </td>
                    <td 
                    className="hori" 
                    onClick={() => 
                    {dispatch(fill12(firstMove, isWin)); 
                    dispatch(setFirstMove(firstMove, tableArray[1][2]));
                    dispatch(trackCurrentPlayer(isWin, tableArray[1][2], playerNames));
                    dispatch(moveIncrement())
                    }}>
                        {tableArray[1][2]}
                    </td>
                </tr>
                <tr>
                    <td 
                    onClick={() => 
                    {dispatch(fill20(firstMove, isWin)); 
                    dispatch(setFirstMove(firstMove, tableArray[2][0]));
                    dispatch(trackCurrentPlayer(isWin, tableArray[2][0], playerNames));
                    dispatch(moveIncrement())
                    }}>
                        {tableArray[2][0]}
                    </td>
                    <td 
                    className="vert" 
                    onClick={() => 
                    {dispatch(fill21(firstMove, isWin)); 
                    dispatch(setFirstMove(firstMove, tableArray[2][1]));
                    dispatch(trackCurrentPlayer(isWin, tableArray[2][1], playerNames));
                    dispatch(moveIncrement())
                    }}>
                        {tableArray[2][1]}
                    </td>
                    <td 
                    onClick={() => 
                    {dispatch(fill22(firstMove, isWin)); 
                    dispatch(setFirstMove(firstMove, tableArray[2][2]));
                    dispatch(trackCurrentPlayer(isWin, tableArray[2][2], playerNames));
                    dispatch(moveIncrement())
                    }}>
                        {tableArray[2][2]}
                    </td>
                </tr>
            </table>
            {!isWin && !isDraw ? <h1>{currentPlayer}'s turn</h1>: null}</div>
            :             
            <div>
                <h1>Set player 1 name:&nbsp;&nbsp;<input type="text" name="player1Name" value={playerNames.player1Name} onChange={handleChangePlayer1}></input></h1>
                <h1>Set player 2 name:&nbsp;&nbsp;<input type="text" name="player2Name" value={playerNames.player2Name} onChange={handleChangePlayer2}></input></h1>
                <h1>{playerNames.player1Name}, choose your move.</h1>
                <button style={{margin:"auto", width:"50%"}} onClick={() => {dispatch(setFirstMove('O')); dispatch(trackCurrentPlayer(isWin, "", playerNames))}}>O</button>
                <button style={{margin:"auto", width:"50%"}} onClick={() => {dispatch(setFirstMove('X')); dispatch(trackCurrentPlayer(isWin, "", playerNames))}}>X</button>
            </div>            
            }
            {isWin && !isDraw ? currentPlayer ===  playerNames.player1Name ? <h1><div>{playerNames.player2Name} Wins<br></br><button onClick={() => dispatch(reset())}>Reset</button></div></h1> : <h1><div>{playerNames.player1Name} Wins<br></br><button onClick={() => dispatch(reset())}>Reset</button></div></h1> : null}
            {isDraw ? <h1><div>Draw<br></br><button onClick={() => dispatch(reset())}>Reset</button></div></h1> : null}
            <MatchHistory />
        </div>
        
    );
}

export default TicTacToe;