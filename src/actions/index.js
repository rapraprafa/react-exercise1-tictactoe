export const fill00 = (value, isWin) => {
    return{
        type: '0,0',
        value: value,
        isWin: isWin
    };
};

export const fill01 = (value, isWin) => {
    return{
        type: '0,1',
        value: value,
        isWin: isWin
    };
};

export const fill02 = (value, isWin) => {
    return{
        type: '0,2',
        value: value,
        isWin: isWin
    };
};

export const fill10 = (value, isWin) => {
    return{
        type: '1,0',
        value: value,
        isWin: isWin
    };
};

export const fill11 = (value, isWin) => {
    return{
        type: '1,1',
        value: value,
        isWin: isWin
    };
};

export const fill12 = (value, isWin) => {
    return{
        type: '1,2',
        value: value,
        isWin: isWin
    };
};

export const fill20 = (value, isWin) => {
    return{
        type: '2,0',
        value: value,
        isWin: isWin
    };
};

export const fill21 = (value, isWin) => {
    return{
        type: '2,1',
        value: value,
        isWin: isWin
    };
};

export const fill22 = (value, isWin) => {
    return{
        type: '2,2',
        value: value,
        isWin: isWin
    };
};

export const setFirstMove = (move, currentValue) => {
    return{
        type: move,
        currentValue: currentValue
    };
};

export const displayWin = (value) => {
    return{
        type: 'displayWin',
        value: value
    }
}

export const trackCurrentPlayer = (isWin, currentValue, playerNames) => {
    return{
        type: 'trackCurrentPlayer',
        isWin: isWin,
        currentValue: currentValue,
        playerNames: playerNames
    }
}

export const reset = () => {
    return{
        type: 'reset'
    }
}

export const changePlayerName = (whatPlayer, value) => {
    return{
        type: 'changePlayerName',
        whatPlayer: whatPlayer,
        value: value
    }
}

export const pushToMatchHistory = (payload) => {
    return {
        type: 'pushToMatchHistory',
        payload: payload
    }
}

export const gameCountIncrement = () => {
    return {
        type: 'gameCountIncrement',
    }
}

export const moveIncrement = () => {
    return {
        type: 'moveIncrement',
    }
}