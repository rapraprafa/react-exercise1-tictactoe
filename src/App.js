import React from 'react';
import './App.css';
import TicTacToe from './TicTacToeComponent/TicTacToe';

function App() {
  return (
    <div>
      <h1>Let's Play Tic Tac Toe!</h1>
      <TicTacToe />
    </div>
  );
}

export default App;
