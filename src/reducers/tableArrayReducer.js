/* eslint-disable no-useless-computed-key */
const tableArrayReducer = (state = [["", "", ""], ["", "", ""], ["", "", ""]], action) => {
    function myArr(i, j) {
        if(action.isWin === true){
            action.value = "";
        }
        var myArr = Object.assign([...state], {
            [i]: Object.assign([...state[i]], {
                [j]: action.value
            })
        })
        return myArr;
    }
    switch (action.type) {
        case '0,0':
            return state[0][0] !== "" ? state : myArr(0,0);
        case '0,1':
            return state[0][1] !== "" ? state : myArr(0,1);
        case '0,2':
            return state[0][2] !== "" ? state : myArr(0,2);
        case '1,0':
            return state[1][0] !== "" ? state : myArr(1,0);
        case '1,1':
            return state[1][1] !== "" ? state : myArr(1,1);
        case '1,2':
            return state[1][2] !== "" ? state : myArr(1,2);
        case '2,0':
            return state[2][0] !== "" ? state : myArr(2,0);
        case "2,1":
            return state[2][1] !== "" ? state : myArr(2,1);
        case '2,2':
            return state[2][2] !== "" ? state : myArr(2,2);
        default:
            return state;
    }
};

export default tableArrayReducer;