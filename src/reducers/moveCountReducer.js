const moveCountReducer = (state = 0, action) => {
    switch(action.type){
        case 'moveIncrement':
            return state + 1;
        default:
            return state;
    }

};

export default moveCountReducer;