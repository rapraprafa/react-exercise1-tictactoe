import tableArrayReducer from './tableArrayReducer';
import firstMoveReducer from './firstMoveReducer';
import playerNamesReducer from './playerNamesReducer';
import displayWinReducer from './displayWinReducer';
import currentPlayerReducer from './currentPlayerReducer';
import matchHistoryReducer from './matchHistoryReducer';
import gameCountReducer from './gameCountReducer';
import moveCountReducer from './moveCountReducer'
import {combineReducers} from 'redux';

const allReducers = combineReducers({
    tableArray: tableArrayReducer,
    firstMove: firstMoveReducer,
    playerNames: playerNamesReducer,
    displayWin: displayWinReducer,
    currentPlayer: currentPlayerReducer,
    matchHistory: matchHistoryReducer,
    gameCount: gameCountReducer,
    moveCount: moveCountReducer
})

const rootReducer = (state, action) => {
    if (action.type === 'reset') {
      state.tableArray = undefined
      state.firstMove = undefined
      state.displayWin = undefined
      state.currentPlayer = undefined
      state.moveCount = undefined
    }
    return allReducers(state, action)
  }

export default rootReducer;