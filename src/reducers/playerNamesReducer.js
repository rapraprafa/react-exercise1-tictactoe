const playerNamesReducer = (state = {player1Name: "Player 1", player2Name: 'Player 2'}, action) => {
    var newState = {};
    switch(action.type){
        case 'changePlayerName':
            if (action.whatPlayer === 'Player 1'){
                newState = {
                    player1Name: action.value,
                    player2Name: state.player2Name
                };
            }
            else{
                newState = {
                    player1Name: state.player1Name,
                    player2Name: action.value
                };
            }
            return newState;
        default:
            return state;
    }
};

export default playerNamesReducer;