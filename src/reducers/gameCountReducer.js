const gameCountReducer = (state = 1, action) => {
    switch(action.type){
        case 'gameCountIncrement':
            return state + 1;
        default:
            return state;
    }
};

export default gameCountReducer;