const displayWinReducer = (state = false, action) => {
    switch(action.type){
        case 'displayWin':
            return action.value;
        default:
            return state;
    }

};

export default displayWinReducer;