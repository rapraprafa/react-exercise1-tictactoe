const matchHistoryReducer = (state = [], action) => {
    switch(action.type){
        case 'pushToMatchHistory':
            state.push(action.payload);
            return state;
        default:
            return state;
    }
};

export default matchHistoryReducer;