

const currentPlayerReducer = (state = '', action) => {
    switch(action.type){
        case 'trackCurrentPlayer':
            if(action.isWin === true){
                return state;
            }
            if(action.currentValue!==""){
                return state;
            }
            return state === action.playerNames.player1Name ? action.playerNames.player2Name : action.playerNames.player1Name;
        default:
            return state;
    }

};

export default currentPlayerReducer;