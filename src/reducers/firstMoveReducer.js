const firstMoveReducer = (state = "", action) => {
        switch(action.type){
            case 'X':
                if (state === ""){
                    return 'X';
                }
                if (action.currentValue!==""){
                    return state;
                }
                return 'O';
            case 'O':
                if (state === ""){
                    return 'O';
                }
                if (action.currentValue!==""){
                    return state;
                }
                return 'X';
            default:
                return state;
        }

};

export default firstMoveReducer;