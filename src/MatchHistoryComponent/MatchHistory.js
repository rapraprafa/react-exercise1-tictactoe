import React from 'react'
import { useSelector } from 'react-redux';

const MatchHistory = () => {
    const matchHistory = useSelector(state => state.matchHistory);

    return (
        <div>
            <table>
                <tr>
                    <th className="tdmatch">Game #</th>
                    <th className="tdmatch">Winner</th>
                    <th className="tdmatch"># of Moves</th>
                </tr>
                {matchHistory.map(match => {
                    return (
                        <tr>
                            <td className="tdmatch">{match.gameCount}</td>
                            <td className="tdmatch">{match.winner}</td>
                            <td className="tdmatch">{match.movesTaken} {match.move === null ? "" : " (" + match.move + ")"}</td>
                        </tr>
                    )
                })}
            </table>
        </div>
    )
}

export default MatchHistory